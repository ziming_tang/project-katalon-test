import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://18.234.164.252:8090/')

WebUI.setText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend03/Page_ProjectBackend/input_Username_username'), 
    'user')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend03/Page_ProjectBackend/input_Password_password'), 
    '1/VWEm4uipk=')

WebUI.click(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend03/Page_ProjectBackend/button_Login'))

WebUI.click(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend03/Page_ProjectBackend/h2_Products'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend03/Page_ProjectBackend/h5_Garden'), 
    'Garden')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend03/Page_ProjectBackend/p_The garden which you can grow everything _43035b'), 
    'The garden which you can grow everything on earth')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend03/Page_ProjectBackend/h6_Price is 2000000 THB'), 
    'Price is 20,000.00 THB')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend03/Page_ProjectBackend/h5_Banana'), 
    'Banana')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend03/Page_ProjectBackend/p_A good fruit with very cheap price'), 
    'A good fruit with very cheap price')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend03/Page_ProjectBackend/h6_Price is 15000 THB'), 
    'Price is 150.00 THB')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend03/Page_ProjectBackend/h5_Orange'), 
    'Orange')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend03/Page_ProjectBackend/p_Nothing good about it'), 
    'Nothing good about it')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend03/Page_ProjectBackend/h6_Price is 28000 THB'), 
    'Price is 280.00 THB')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_ProjectBackend03/Page_ProjectBackend/h5_Papaya'), 'Papaya')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend03/Page_ProjectBackend/p_Use for papaya salad'), 
    'Use for papaya salad')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend03/Page_ProjectBackend/h6_Price is 1200 THB'), 
    'Price is 12.00 THB')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_ProjectBackend03/Page_ProjectBackend/h5_Rambutan'), 'Rambutan')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend03/Page_ProjectBackend/p_An expensive fruit from the sout'), 
    'An expensive fruit from the sout')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend03/Page_ProjectBackend/h6_Price is 2000 THB'), 
    'Price is 20.00 THB')

WebUI.closeBrowser()

