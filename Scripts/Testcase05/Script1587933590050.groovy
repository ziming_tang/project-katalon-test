import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://18.234.164.252:8090/')

WebUI.setText(findTestObject('Page_ProjectBackend/Page_ProjectBackend05/Page_ProjectBackend/input_Username_username'), 'user')

WebUI.setEncryptedText(findTestObject('Page_ProjectBackend/Page_ProjectBackend05/Page_ProjectBackend/input_Password_password'), 
    '1/VWEm4uipk=')

WebUI.click(findTestObject('Page_ProjectBackend/Page_ProjectBackend05/Page_ProjectBackend/button_Login'))

WebUI.click(findTestObject('Page_ProjectBackend/Page_ProjectBackend05/Page_ProjectBackend/button_add to cart'))

WebUI.click(findTestObject('Page_ProjectBackend/Page_ProjectBackend05/Page_ProjectBackend/a_Carts            1'))

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_ProjectBackend05/Page_ProjectBackend/th_1'), '1')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_ProjectBackend05/Page_ProjectBackend/td_Banana'), 'Banana')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_ProjectBackend05/Page_ProjectBackend/td_150 THB'), '150 THB')

WebUI.click(findTestObject('Page_ProjectBackend/Page_ProjectBackend05/Page_ProjectBackend/input_Banana_amount'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_ProjectBackend05/Page_ProjectBackend/p_Total price  150 THB'), 
    'Total price: 150 THB')

WebUI.setText(findTestObject('Page_ProjectBackend/Page_ProjectBackend05/Page_ProjectBackend/input_Banana_amount_2'), '3')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_ProjectBackend05/Page_ProjectBackend/p_Total price  450 THB'), 
    'Total price: 450 THB')

WebUI.click(findTestObject('Page_ProjectBackend/Page_ProjectBackend05/Page_ProjectBackend/button_confirm'))

WebUI.acceptAlert()

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Page_ProjectBackend05/Page_ProjectBackend/div_Well done You successfully added the tr_c24d23'), 
    'Well done! You successfully added the transaction.')

WebUI.closeBrowser()

