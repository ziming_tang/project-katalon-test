import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import org.openqa.selenium.By as By

import org.openqa.selenium.WebDriver as WebDriver

import org.openqa.selenium.WebElement as WebElement

import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.openBrowser('')

WebUI.navigateToUrl('http://18.234.164.252:8090/')

WebUI.setText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend07/Page_ProjectBackend/input_Username_username'), 
    'admin')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend07/Page_ProjectBackend/input_Password_password'), 
    'RAIVpflpDOg=')

WebUI.click(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend07/Page_ProjectBackend/button_Login'))

WebUI.click(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend07/Page_ProjectBackend/a_Total Transaction'))

WebDriver driver = DriverFactory.getWebDriver()
'Expected value from Table'
String ExpectedValue = "Pay Talk";
'To locate table'
WebElement Table = driver.findElement(By.xpath("//table/tbody"))
'To locate rows of table it will Capture all the rows available in the table'
List<WebElement> rows_table = Table.findElements(By.tagName('tr'))
'To calculate no of rows In table'
int rows_count = rows_table.size()

'Loop will execute for all the rows of the table'
Loop:
for (int row = 0; row < rows_count; row++) {
'To locate columns(cells) of that specific row'
List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName('td'))

'To calculate no of columns(cells) In that specific row'
int columns_count = Columns_row.size()

println((('Number of cells In Row ' + row) + ' are ') + columns_count)

'Loop will execute till the last cell of that specific row'
for (int column = 0; column < columns_count; column++) {
'It will retrieve text from each cell'
String celltext = Columns_row.get(column).getText()

println((((('Cell Value Of row number ' + row) + ' and column number ') + column) + ' Is ') + celltext)

'Checking if Cell text is matching with the expected value'
if (celltext == ExpectedValue) {
'Getting the Country Name if cell text i.e Company name matches with Expected value'
println('Text present in row number 3 is: ' + Columns_row.get(2).getText())

'After getting the Expected value from Table we will Terminate the loop'
break Loop;
}
}
}


/*WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend07/Page_ProjectBackend/td_1'), 
    '1')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend07/Page_ProjectBackend/td_Garden Papaya'), 
    'Garden, Papaya')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend07/Page_ProjectBackend/td_20120 THB'), 
    '20,120 THB')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend07/Page_ProjectBackend/td_2'), 
    '2')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend07/Page_ProjectBackend/td_Banana Garden Banana Rambutan'), 
    'Banana, Garden, Banana, Rambutan')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend07/Page_ProjectBackend/td_60570 THB'), 
    '60,570 THB')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend07/Page_ProjectBackend/td_3'), 
    '3')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend07/Page_ProjectBackend/td_Orange'), 
    'Orange')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend07/Page_ProjectBackend/td_280 THB'), 
    '280 THB')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend07/Page_ProjectBackend/td_4'), 
    '4')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend07/Page_ProjectBackend/td_Orange Banana'), 
    'Orange, Banana')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend07/Page_ProjectBackend/td_710 THB'), 
    '710 THB')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/Page_ProjectBackend07/Page_ProjectBackend/p_Total price  81680 THB'), 
    'Total price: 81,680 THB')*/

WebUI.closeBrowser()

