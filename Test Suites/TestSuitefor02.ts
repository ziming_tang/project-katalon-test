<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TestSuitefor02</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>98774eae-3e47-4fc7-b6e7-2ed10f98dd7e</testSuiteGuid>
   <testCaseLink>
      <guid>bfb86da6-7d56-4598-b54f-421d75bcbcdd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Testcase2-1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f656ce17-aef2-4999-a3b6-eed6a6650850</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Testcase2-2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>69965bfb-4b67-4a06-a7c8-baa87b72726f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Testcase2-3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>503d2208-eb27-4710-b06d-ca9982ab1a2d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Testcase2-4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b8d0605-3269-4962-a3f4-713a508ea0d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Testcase2-5</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
